var timeline1 = new GreenSockGlobals.TimelineMax({repeat:-1, yoyo: true});
var timeline2 = new GreenSockGlobals.TimelineMax();
var timeline3 = new GreenSockGlobals.TimelineMax();

var roof = document.querySelector('svg g#roof'),
    base = document.querySelector('svg g#base'),
    your = document.querySelector('svg g#your'),
    home = document.querySelector('svg g#home'),
    allHouseSVG = document.querySelector('svg#house-logo > g#housesvg'),
    allLivingRoom = document.querySelector('div.livingroom'),
    allKitchen = document.querySelector('div.kitchen'),
    allBedRoom = document.querySelector('div.bedroom'),
    clickmeButton = document.querySelector('div#clickmeButton'),
    clickMeAgainButton = document.querySelector('div#clickMeAgain'),
    body = document.querySelector('body');

// console.log(roof,base,your,home);
// console.log(allBedRoom,allKitchen,allLivingRoom);
// console.log(clickmeButton);

let livingroomDiv = document.querySelector('.livingroom');
let bedroomDiv = document.querySelector('.bedroom');
let kitchenDiv = document.querySelector('.kitchen');
let startText = document.querySelectorAll('div#startText span');
let endText = document.querySelectorAll('div#endText span');

// this are the livingroom forniture
let livingroom = {
sofaL: document.querySelector('div.livingroom img#LR-SL'),
sofaR: document.querySelector('div.livingroom img#LR-SR'),
sessel: document.querySelector('div.livingroom img#LR-S'),
teppich: document.querySelector('div.livingroom img#LR-T'),
uhr: document.querySelector('div.livingroom img#LR-U'),
lampe: document.querySelector('div.livingroom img#LR-L'),
kommode: document.querySelector('div.livingroom img#LR-K'),
couchtisch: document.querySelector('div.livingroom img#LR-CT')}
let LRForniture = Object.values(livingroom);

// this are the kitchen forniture
let kitchen = {
arbeitsbank: document.querySelector('div.kitchen img#K-AB'),
abszugshaube: document.querySelector('div.kitchen img#K-AH'),
bild: document.querySelector('div.kitchen img#K-B'),
kuehlscrank: document.querySelector('div.kitchen img#K-Ks'),
kuechenschrankL: document.querySelector('div.kitchen img#K-KSL'),
kuechenschrankR: document.querySelector('div.kitchen img#K-KSR'),
lampe: document.querySelector('div.kitchen img#K-L'),
stuhlL: document.querySelector('div.kitchen img#K-SL'),
stuhlR: document.querySelector('div.kitchen img#K-SR'),
tisch: document.querySelector('div.kitchen img#K-T')}
let KForniture = Object.values(kitchen);

// this are the bedroom forniture
let bedroom = {
bett: document.querySelector('div.bedroom img#BR-B')/*allBedRoom[1]*/,
kommode: document.querySelector('div.bedroom img#BR-K'),
stuhl: document.querySelector('div.bedroom img#BR-S'),
schreibtisch: document.querySelector('div.bedroom img#BR-ST'),
tisch: document.querySelector('div.bedroom img#BR-T'),
regal: document.querySelector('div.bedroom img#BR-R')}
let BRForniture = Object.values(bedroom);


// console.log(LRForniture, KForniture, BRForniture);


// charge by open of the page the animations and start the 1 animation
document.addEventListener('DOMContentLoaded', function() {
  hideAll();
  initAll();
  timeline1.play();
});

// start by click evente the 2 animation
allHouseSVG.addEventListener('click', function(){
   hideAll();
   timeline2.play(0)
}, {once : true})

// start by click event the 3 animation, and stop the second one
function closeAnimation(){
// console.log('finish');
  allHouseSVG.addEventListener('click', function(){
     timeline2.pause();
     timeline3.play(0)
  }, {once : true})
}

// hide all the animations, for a clean start
function hideAll() {
  livingroomDiv.style.opacity = 0;
  bedroomDiv.style.opacity = 0;
  kitchenDiv.style.opacity = 0;
  kitchenDiv.style.opacity = 0;

  endText.forEach(function (element){
    element.style.opacity = 0;
  })
  startText.forEach(function (element){
    element.style.opacity = 0;
  })

  timeline1.pause();
  timeline2.pause();
  timeline3.pause();
}


// Start to charge all the animations
function initAll() {
  initStartAnimation();
  initOpenhouse();
  endLogoAnimation();
}

// Start first animation, -- animation waits in the end an event (click on the house) --
function initStartAnimation(){
  timeline1
    .set(allBedRoom, { opacity: 0 })
    .set(allKitchen, { opacity: 0 })
    .set(allLivingRoom, { opacity: 0 })
    .set(clickmeButton, {
      opacity: 1,
      x: 129,
      y: -253,
      transformOrigin: '0% 50%',
      rotation:-20
    })
// start pulsing effect of the house/logo
    .fromTo(allHouseSVG,1, {
      scale:1.7,
      transformOrigin: '50% 50%',
      ease: Elastic.easeOut.config(1, 0.3),
      repeat: 2,
      }, {
      scale:1, ease: Elastic.easeOut.config(1, 0.4),
    })
}

// start second animation
function initOpenhouse(){
timeline2
// finish the animation of the first animation, after the click
    .to(allHouseSVG, 0.4, { scale: 0.2, transformOrigin: '50% 50%', })
    .to(clickmeButton, 0.6, { scale: 0, y:-300, rotation: 40, transformOrigin: '30% 0%', ease: Power4.easeOut },'-=0.2')
// prepare the scene with setting elements
    .set(allKitchen, { y: 3000, opacity: 1 })
    .set(allLivingRoom, { y: -9999, opacity: 1 })
    .set(allBedRoom, { x: 3000, opacity: 1 })
    .set(LRForniture, { x: -48, y: 52, opacity: 0 })
    .set(KForniture, { x: -48, y: -240, opacity: 0 })
    .set(BRForniture, { x: -160, y: -89, opacity: 0 })
// start the first frames of the animation
    .fromTo(allHouseSVG, 0.7, { delay:0.6, rotation: -1440, scale: 0.3 },
      { rotation: 0, scale:1, ease: Back.easeOut.config(1.7) })
    .to(roof, 1, { transformOrigin: '0% 100%', rotation: -70, ease: Back.easeOut.config(1.7) })

    .set(your, { x: 154, y: 475, scale: 0.2, rotation: -60, transformOrigin: '50% 50%', display: 'block', opacity: 1, ease: Power1.easeIn },'-=0.5')
    .to(your, 0.6, { x: 210, y: 160, scale: 0.5, rotation: -15, transformOrigin: '50% 50%' },'-=0.5')
    .to(your, 0.6, { x: 210, y: 150, scale: 0.6, rotation: -5, transformOrigin: '50% 50%' },'-=0.5')

    .to(allHouseSVG, 0.5, { rotation: -10, transformOrigin: '50% 50%', x: -200, ease: Back.easeOut.config(1) },'-=0.4')

    .to(your, 0.6, { x: 210, y: 140, scale: 0.65, rotation: 0, transformOrigin: '50% 50%' },'-=0.6')
    .to(your, 0.6, { x: 210, y: 150, scale: 0.7, rotation: 10, transformOrigin: '50% 50%' },'-=0.5')
    .to(your, 0.6, { x: 210, y: 180, scale: 0.8, rotation: 17, transformOrigin: '50% 50%' },'-=0.4')
    .to(your, 0.6, { x: 215, y: 252, scale: 0.8, rotation: 30, transformOrigin: '50% 50%', ease: Bounce.easeOut },'-=0.6')

    .set(home, { x: -150, y: 210, scale: 0.2, rotation: -70, transformOrigin: '50% 50%', display: 'block' },'-=0.6')

    .to(your, 0.4, { x: 154, y: 475, scale: 1, rotation: 0, transformOrigin: '50% 50%', ease: Bounce.easeOut },'-=0.3')

    .to(allHouseSVG, 0.5, { rotation: 0, transformOrigin: '50% 50%', ease: Back.easeInOut.config(4) },"-=0.4")

    .to(home, 0.4, { x: -90, y: 50, scale: 0.5, rotation: -37, transformOrigin: '50% 50%' },'-=0.8')
    .to(home, 0.4, { x: -20, y: 50, scale: 0.5, rotation: -20, transformOrigin: '50% 50%' },'-=0.7')
    .to(home, 0.4, { x: 30, y: 50, scale: 0.62, rotation: 10, transformOrigin: '50% 50%' },'-=0.5')
    .to(home, 0.4, { x: 70, y: 50, scale: 0.64, rotation: 30, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 130, y: 70, scale: 0.66, rotation: 35, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 180, y: 100, scale: 0.68, rotation: 40, transformOrigin: '50% 50%' },'-=0.3')
    .to(home, 0.4, { x: 200, y: 100, scale: 0.7, rotation: 40, transformOrigin: '50% 50%' },'-=0.3')
    .to(home, 0.4, { x: 215, y: 100, scale: 0.72, rotation: 40, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 230, y: 100, scale: 0.74, rotation: 40, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 245, y: 159, scale: 0.76, rotation: 42, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 255, y: 178, scale: 0.78, rotation: 42, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 260, y: 175, scale: 1,  rotation: 42, transformOrigin: '50% 50%' },'-=0.4')
    .to(home, 0.4, { x: 155, y: 475, scale: 1, rotation: 0, transformOrigin: '50% 50%', ease: Bounce.easeOut },'-=0.3')

    .to(roof, 0.6, { transformOrigin: '0% 100%', rotation: 0, ease: Back.easeOut.config(1.7) },'+=0.3')
    .to(home, 2, { scale: 100, rotation: 3, opacity: -10, ease: Power3.easeIn, y: -500, delay: 0.5 },'-=0.3')

    .to(your, 2, { scale: 100, rotation: 3, opacity: -10, ease: Power3.easeIn, y: -500, delay: 0 },'-=1.5')

    .to(allHouseSVG, 1, { x: -62, ease: Back.easeInOut.config(1.7) },'-=1')
    .to(allKitchen, 1, { y: -84, ease: Bounce.easeOut },'-=1.4')
    .to(allBedRoom, 1, { x: 270, ease: Bounce.easeOut },'-=1.2')
    .to(allLivingRoom, 1, { y:-70, ease: Bounce.easeOut }, '-=1.2')
    .to(roof, 1, { transformOrigin: '0% 100%', rotation: -70, ease: Back.easeOut.config(1.7) }, '-=0.5')

  // ANIMATION FORNITURE OF THE  LIVINGROOM
    .set(livingroom.sofaL, { opacity: 1 },'-=0.6')
    .to(livingroom.sofaL, 3, { bezier: [
      { x: -48, y: 52 },
      { x: -54, y: -185 },
      { x: 39, y:-77 }],
      scale:3.3, ease: Elastic.easeOut.config(1, 0.3) },'-=0.6')

    .set(livingroom.sofaR, { opacity: 1 },'-=2.7')
    .to(livingroom.sofaR, 3, { bezier: [
      { x: -48, y: 52 },
      { x: -15, y: -212 },
      { x: 92, y: -79 }],
      scale:3.5, ease: Elastic.easeOut.config(0.8, 0.4) },'-=2.7')

    .set(livingroom.sessel, { opacity: 1 },'-=2.7')
    .to(livingroom.sessel, 3, { bezier: [
      { x: -48, y: 52 },
      { x: 85, y: 53 },
      { x: 90, y: -47 }],
      scale:3.2, ease: Elastic.easeOut.config(0.8, 0.4) },'-=2.7')

    .set(livingroom.teppich, { opacity: 1 },'-=2.7')
    .to(livingroom.teppich, 3, { bezier: [
      { x: -48, y: 52 },
      { x: -46, y: -164 },
      { x: 66, y: -54 }],
      scale:8, ease: Elastic.easeOut.config(0.8, 0.7) },'-=2.7')

    .set(livingroom.uhr, { opacity: 1 },'-=2.7')
    .to(livingroom.uhr, 3, { bezier: [
      { x: -48, y: 52 },
      { x: 85, y: 53 },
      { x: 124, y: -107 }],
      scale:1.7, ease: Elastic.easeOut.config(0.8, 0.7) },'-=2.7')

    .set(livingroom.lampe, { opacity: 1 },'-=2.9')
    .to(livingroom.lampe, 3, { bezier: [
      { x: -48, y: 52 },
      { x: 0, y: -12 },
      { x: 10, y: -85 }],
      scale:1.7, ease: Elastic.easeOut.config(0.8, 0.7) },'-=2.9')

    .set(livingroom.kommode, { opacity: 1 },'-=2.8')
    .to(livingroom.kommode, 3, { bezier: [
      { x: -48, y: 52 },
      { x: -19, y: -40 },
      { x: 121, y: -75 }],
      scale:2.2, ease: Elastic.easeOut.config(0.8, 0.7) },'-=2.8')

    .set(livingroom.couchtisch, { opacity: 1 },'-=2.8')
    .to(livingroom.couchtisch, 3, { bezier: [
      { x: -48, y: 52 },
      { x: 66, y: -35 },
      { x: 65, y: -58 }],
      scale:2.8, ease: Elastic.easeOut.config(0.8, 0.7)},'-=3')
// ANIMATION FORNITURE OF THE KITCHEN
    .set(kitchen.arbeitsbank, { opacity: 1 },'-=2.9')
    .to(kitchen.arbeitsbank, 3, { bezier: [
      { x: -48, y: -240 },
      { x: -50, y: -120 },
      { x: 46, y: -85 }],
      scale:6.6, ease: Elastic.easeOut.config(1, 0.3)},'-=2.9')

    .set(kitchen.abszugshaube, { opacity: 1 },'-=2.6')
    .to(kitchen.abszugshaube, 3, { bezier: [
      { x: -48, y: -240 },
      { x: 75,  y: -162 },
      { x: 45, y: -132 }],
      scale:3.6, ease: Elastic.easeOut.config(0.8, 0.4)},'-=2.6')

    .set(kitchen.bild, { opacity: 1 },'-=2.7')
    .to(kitchen.bild, 3, { bezier: [
      { x: -48, y: -240 },
      { x: 89, y: -202 },
      { x: 119, y: -113 }],
      scale:2, ease: Elastic.easeOut.config(0.8, 0.4)},'-=2.7')

    .set(kitchen.kuehlscrank, { opacity: 1 },'-=2.8')
    .to(kitchen.kuehlscrank, 3, { bezier: [
      { x: -48, y: -240 },
      { x: 11, y: -174 },
      { x: 11, y: -84 }],
      scale:3.3, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.8')

    .set(kitchen.kuechenschrankL, { opacity: 1 },'-=2.8')
    .to(kitchen.kuechenschrankL, 3, { bezier: [
      { x: -48, y: -240 },
      { x: -53, y: -154 },
      { x: 8, y: -114 }],
      scale:2.6, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.8')

    .set(kitchen.kuechenschrankR, { opacity: 1 },'-=2.7')
    .to(kitchen.kuechenschrankR, 3, { bezier: [
      { x: -48, y: -240 },
      { x: 88, y: -180 },
      { x: 28, y: -120 }],
      scale:2.6, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.74')

    .set(kitchen.lampe, { opacity: 1 },'-=2.6')
    .to(kitchen.lampe, 3, { bezier: [
      { x: -48, y: -240 },
      { x: 64,  y: -184 },
      { x: 94, y: -113 }],
      scale:2.2, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.6')

    .set(kitchen.stuhlL, { opacity: 1 },'-=2.7')
    .to(kitchen.stuhlL, 3, { bezier: [
      { x: -48, y: -240 },
      { x: -38, y: -121},
      { x: 82,  y: -51 }],
      scale:2.3, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.7')

    .set(kitchen.stuhlR, { opacity: 1 },'-=2.7')
    .to(kitchen.stuhlR, 3, { bezier: [
      { x: -48, y: -240 },
      { x:  64, y: -184 },
      { x: 107, y: -69 }],
      scale:2.2, ease: Elastic.easeOut.config(0.8, 0.7)  },'-=2.7')

    .set(kitchen.tisch, { opacity: 1 },'-=2.6')
    .to(kitchen.tisch, 3, { bezier: [
      { x: -48, y: -240 },
      { x: -53, y: -154 },
      { x: 95, y: -58 }],
      scale:3.8, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.6')

// ANIMATION FORNITURE OF THE BEDROOM
    .set(bedroom.bett, { opacity: 1 },'-=2.9')
    .to(bedroom.bett, 3, { bezier: [
      { x: -160, y: -89 },
      { x: 52, y: 14 },
      { x: 84, y: -96 }],
      scale:7.6, ease: Elastic.easeOut.config(0.8, 0.4)},'-=2.9')

    .set(bedroom.kommode, { opacity: 1 },'-=2.7')
    .to(bedroom.kommode, 3, { bezier: [
      { x: -160, y: -89 },
      { x: 85, y: 53 },
      {x: 114, y: -73 }],
      scale:4, ease: Elastic.easeOut.config(0.8, 0.4)},'-=2.7')

    .set(bedroom.stuhl, { opacity: 1 },'-=2.6')
    .to(bedroom.stuhl, 3, {bezier: [
      { x: -160, y: -89 },
      { x: -89, y: -134 },
      { x: 29, y: -65}],
      scale:3, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.7')

    .set(bedroom.schreibtisch, { opacity: 1 },'-=2.7')
    .to(bedroom.schreibtisch, 3, { bezier: [
      { x: -160, y: -89},
      { x: 48, y: -214},
      { x: 21, y: -70 }],
      scale:4.7, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.6')

    .set(bedroom.tisch, { opacity: 1 },'-=2.8')
    .to(bedroom.tisch, 3, { bezier: [
      { x: -160, y: -89 },
      { x: -49, y: -160 },
      { x: 68, y: -41 }],
      scale:5.6, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.7')

    .set(bedroom.regal, { opacity: 1},'-=2.5')
    .to(bedroom.regal, 3, {bezier: [
      { x: -160, y: -89 },
      { x: -49, y: -160 },
      { x: 13, y: -103 }],
      scale:3.6, ease: Elastic.easeOut.config(0.8, 0.7)},'-=2.5')
// ending the animation closing the roof of the house...
    .to(roof, 1, { transformOrigin: '0% 100%', rotation: -0, ease: Back.easeOut.config(1.7),},'-=3.1')

    .fromTo(roof, 0.5, { transformOrigin: '0% 100%', rotation: -0, ease: Back.easeOut.config(1.7)},
      {transformOrigin: '0% 100%', rotation: -10, ease: Back.easeOut.config(1.7), repeat: -1, yoyo:true,}, '-=1.5')
// on clicking again starts the last part of the animation
    .fromTo(clickMeAgainButton, 1, { rotation: -30, scale: 0, x:125, y:-450, transformOrigin: '30% 0%', ease: Power4.easeOut },
      { opacity: 1, scale: 1, x:19, y:-450, rotation: 20, transformOrigin: '30% 0%', ease: Power4.easeOut },'-=0.2')
// call of a function that start a new click event
    .set(allHouseSVG, {
      onComplete: closeAnimation
    })
 }

// start last part of animation
function endLogoAnimation(){
 //Close Animation
 timeline3
 // start last animation
   .to(clickMeAgainButton, 1, { rotation: 0, scale: 0, x: 126, y: -426, transformOrigin: '30% 0%', ease: Power4.easeOut },'-=0.2')
   .to(roof, 1, { transformOrigin: '0% 100%', rotation: -100, ease: Back.easeOut.config(1.7)},'-=0.9')
//
// ANIMATION FORNITURE OF THE LIVINGROOM
   .to(livingroom.sofaL, 1, { bezier: [
     { x: 39, y:-77 },
     { x: -15, y: -62 },
     { x: -41, y: 79 }],
     delay:0.3, scale:0, ease: Power4.easeInOut},'-=0.8')

   .to(livingroom.sofaR, 1, { bezier: [
     { x: 92, y: -79 },
     { x: 61, y: 25 },
     { x: -41, y: 79 }],
     scale:0, ease: Power4.easeInOut},'-=0.7')

   .to(livingroom.sessel, 1, { bezier: [
     { x: 90, y: -47 },
     { x: 0, y: 2 },
     { x: -41, y: 79 }],
   scale:0, ease: Power4.easeInOut},'-=0.9')

   .to(livingroom.teppich, 1, { bezier: [
     { x: 66, y: -54 },
     { x: -24, y: -24 },
     { x: -41, y: 79 }],
     scale:0, ease: Power4.easeInOut},'-=0.7')

   .to(livingroom.uhr, 1, { bezier: [
     { x: 124, y: -107 },
     { x: 34, y: 4 },
     { x: -41, y: 79 }],
     scale:0, ease: Power4.easeInOut},'-=0.8')

   .to(livingroom.lampe, 1, { bezier: [
     { x: 10, y: -85 },
     { x: 0, y: 2 },
     { x: -41, y: 79 }],
   scale:0, ease: Power4.easeInOut},'-=0.9')

   .to(livingroom.kommode, 1, { bezier: [
     { x: 121, y: -75 },
     { x: 61, y: 25 },
     { x: -41, y: 79 }],
     scale:0, ease: Power4.easeInOut},'-=0.9')

   .to(livingroom.couchtisch, 1, { bezier: [
     { x: 65, y: -58 },
     { x: -15, y: -62 },
     { x: -41, y: 79 }],
   scale:0, ease: Power4.easeInOut},'-=0.8')
// ANIMATION FORNITURE OF THE KITCHEN
   .to(kitchen.arbeitsbank,1, { bezier: [
     { x: -48, y: -240 },
     { x: 95, y: -208},
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.6')

   .to(kitchen.abszugshaube, 1, { bezier: [
     { x: 45, y: -132 },
     { x: 22,  y: -171 },
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.5')

   .to(kitchen.bild, 1, { bezier: [
     { x: 119, y: -113 },
     { x: -55, y: -153 },
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.6')

   .to(kitchen.kuehlscrank, 1, { bezier: [
     { x: 11, y: -84 },
     { x: -36,  y: -43 },
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.5')

   .to(kitchen.kuechenschrankL, 1, { bezier: [
     { x: 8, y: -114 },
     { x: 95, y: -208 },
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.7')

   .to(kitchen.kuechenschrankR, 1, { bezier: [
     { x: 28, y: -120 },
     {  x: -55, y: -153 },
     { x: -48, y: -220 }],
   scale:0, ease: Power2.easeInOut},'-=1.6')

   .to(kitchen.lampe, 1, { bezier: [
     { x: 94, y: -113 },
     { x: 64,  y: -243 },
     { x: -48, y: -220 }],
    scale:0, ease: Power2.easeInOut},'-=1.3')

   .to(kitchen.stuhlL, 1, { bezier: [
     { x: 82,  y: -51 },
     { x: -78, y: -121},
     { x: -48, y: -220 }],
    scale:0, ease: Power2.easeInOut},'-=1.6')

   .to(kitchen.stuhlR, 1, { bezier: [
     { x: 107, y: -69 },
     { x:  77, y: -256 },
     { x: -48, y: -220 }],
    scale:0, ease: Power2.easeInOut},'-=1.4')

   .to(kitchen.tisch, 1, { bezier: [
     { x: 95, y: -58 },
     { x: 1, y: -148 },
     { x: -48, y: -220 }],
  scale:0, ease: Power2.easeInOut},'-=1.5')

// ANIMATION FORNITURE OF THE BEDROOM
   .to(bedroom.bett, 1, { bezier: [
     { x: 84, y: -96 },
     { x: -56, y: -23 },
     {x: -135, y: -61 }],
    scale:0, ease: Power4.easeInOut},'-=1.9')

   .to(bedroom.kommode, 1, { bezier: [
     {x: 114, y: -73 },
     { x: -31, y: 125 },
     {x: -135, y: -61 }],
   scale:0, ease: Power4.easeInOut},'-=1.7')

   .to(bedroom.stuhl, 1, {bezier: [
     { x: 29, y: -65},
     { x: -49, y: -50 },
     { x: -135, y: -61 }],
   scale:0, ease: Power4.easeInOut},'-=1.8')

   .to(bedroom.schreibtisch, 1, { bezier: [
     { x: 21, y: -70 },
     { x: -42, y: -141},
     { x: -135, y: -61 }],
   scale:0, ease: Power4.easeInOut},'-=1.7')

   .to(bedroom.tisch, 1, { bezier: [
     { x: 68, y: -41 },
     { x: -49, y: -103 },
     { x: -135, y: -61 }],
   scale:0, ease: Power4.easeInOut},'-=1.9')

   .to(bedroom.regal, 1, {bezier: [
     { x: 13, y: -103 },
     { x: -42, y: -103 },
     { x: -135, y: -61 }],
   scale:0, ease: Power4.easeInOut},'-=1.8')

   .to(livingroomDiv, 1, { bezier: [
     {x: 60, y:-30 },
     {x: 70, y:100 }],
     scale: 0, ease: Power2.easeInOut},'-=0.9')
   .to(bedroomDiv, 1, {bezier: [
     {x: 194, y:-123 },
     {x: 71, y: -61}],
     scale: 0, ease: Power2.easeInOut},'-=0.7')
   .to(kitchenDiv, 1, {bezier: [
     {x: 78, y: -144},
     {x: 68, y: -244}],
     scale: 0, ease: Power2.easeInOut},'-=0.8')
   .to(roof, 1, { transformOrigin: '0% 100%', rotation: 0, ease: Back.easeOut.config(1.7)},'-=0.5')
   .to(allHouseSVG.parentNode, 1, {scale: 1, x: -602, y: -249 })
   .to(roof, 0.4,{ rotation: 0},'-=1.3')

   .staggerFromTo('#startText span', 0.6, {autoAlpha: 0, rotation: -100, scale: 1.4}, {rotation: 0, autoAlpha: 1, scale: 1}, 0.1, '-=1.0')
   .staggerFromTo('#startText span p', 0.6, {autoAlpha: 0, rotation: -100, scale: 1.4, x: -100,y:100}, {rotation: 0, autoAlpha: 1, scale: 1,x: 0, y: 150,}, 0.1, '-=0.7')
   .to('#startText span p', 1 ,{ delay: 0.5, opacity:0, scale:0})

   .staggerFromTo('#endText span', 0.6, {autoAlpha: 0, rotation: -100, scale: 1.4}, {rotation: 0, autoAlpha: 1, scale: 1}, 0.1, '-=1')
   .staggerFromTo('#endText span p.upDown', 0.6, {autoAlpha: 0, rotation: -100, scale: 1.4, x: -100,y: 90}, {rotation: 0, autoAlpha: 1, scale: 1,x: 0, y: 150,}, 0.1,'-=1')
   .staggerFromTo('#endText span p.downUp', 0.6, {autoAlpha: 0, rotation: -100, scale: 1.4, x: -100,y: 250}, {rotation: 0, autoAlpha: 1, scale: 1,x: 0, y: 150,}, 0.1, '-=2.4')
   .to('#endText', 0.9, {delay: 1, rotation: 0, autoAlpha: 0, scale: 0})

   .to('.animationBox', 3, {autoAlpha: 0})
   .set('.animationBox', { display: 'none'})
// Finishing animation by "unlocking" the page, and show the content, and make again possible to scroll the page.
   .set(body, { css:{ overflow: 'inherit'}})
 }
